REPOS_FILE := repos.json
#REPO_URLS := $(shell cat $(REPOS_FILE))

# Define the Helm chart dependency details
# CHART_NAME := my-chart
DEPENDENCY_NAME := shared-template
NEW_DEPENDENCY_VERSION := 1.2.14

# Define the GitHub username and personal access token for authentication
GITLAB_TOKEN := glpat-5pXvT9eE2D_RZHMKBwct

# Define the directory to clone repositories
CLONE_DIR := repos

.PHONY: pr

# Define a target to update the Helm chart dependency and create PR
pr:
	@mkdir -p $(CLONE_DIR)
	@jq -r '.repos[] | .chart_name, .url, .project_id' $(REPOS_FILE) | while read -r chart_name; do \
        read -r url; \
        read -r project_id; \
		repo_name=$$(basename $$url .git); \
		echo "Cloning $$repo_name..."; \
        git clone $$url $(CLONE_DIR)/$$repo_name; \
        git fetch --all; \
        cd $(CLONE_DIR)/$$repo_name; \
        git checkout develop; \
        git switch -c shared-template-new-version; \
        echo "Updating dependency in $$repo_name"; \
        sed -E -i '' '/$(DEPENDENCY_NAME)/,/repository/ s/^([[:space:]]*version:[[:space:]]*)[0-9]+\.[0-9]+\.[0-9]+/\1$(NEW_DEPENDENCY_VERSION)/' charts/$$chart_name/Chart.yaml; \
        git commit -am "Update $(DEPENDENCY_NAME) dependency to $(NEW_DEPENDENCY_VERSION)"; \
        git push origin shared-template-new-version; \
        echo "Creating PR for $$repo_name"; \
        curl --request POST --header "PRIVATE-TOKEN: $(GITLAB_TOKEN)" \
            --data "source_branch=shared-template-new-version" \
            --data "target_branch=develop" \
            --data "title=Update $(DEPENDENCY_NAME) dependency" \
            --data "description=Update $(DEPENDENCY_NAME) dependency to $(NEW_DEPENDENCY_VERSION)" \
            --data "remove_source_branch=true" \
            https://gitlab.com/api/v4/projects/$$project_id/merge_requests; \
        cd ..; \
    done


    